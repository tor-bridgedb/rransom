# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Christian Fromme <kaner@strace.org>, 2010
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-09-29 14:16+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: lib/bridgedb/I18n.py:21
msgid "Here are your bridge relays: "
msgstr "Hier sind Ihre Bridge Relays: "

#: lib/bridgedb/I18n.py:23
msgid ""
"Bridge relays (or \"bridges\" for short) are Tor relays that aren't listed\n"
"in the main directory. Since there is no complete public list of them,\n"
"even if your ISP is filtering connections to all the known Tor relays,\n"
"they probably won't be able to block all the bridges."
msgstr ""
"Sogenannte \"Bridge Relays\" (oder kurz \"Bridges\" ) sind Tor Relays, die\n"
"nicht in den oeffentlich publizierten Relay-Listen auftauchen. Daurch, dass\n"
"die Bridges der Oeffentlichkeit nicht bekannt ist, sind ISPs nicht dazu in\n"
"der Lage, alle Tor Relays zu filtern."

#: lib/bridgedb/I18n.py:28
msgid ""
"To use the above lines, go to Vidalia's Network settings page, and click\n"
"\"My ISP blocks connections to the Tor network\". Then add each bridge\n"
"address one at a time."
msgstr ""
"Um die oben genannten Zeilen zu nutzen, sollten Sie in Vidalia's \"Network\n"
"Settings\" gehen und den Menuepunkt \"Mein ISP blockt Verbindungen zum Tor\n"
"Netzwerk\" anklicken. Anschliessend koennen sie die Bridge-Adressen\n"
"hinzufuegen."


#: lib/bridgedb/I18n.py:32
msgid ""
"Configuring more than one bridge address will make your Tor connection\n"
"more stable, in case some of the bridges become unreachable."
msgstr ""
"Mehr als eine Brigde-Adresse zu verwenden fuehrt zu einer stabileren\n"
"Verbindung. Es kann immer sein, dass mal eine Bridge kurzzeitig nicht\n"
"erreichbar ist."

#: lib/bridgedb/I18n.py:35
msgid ""
"Another way to find public bridge addresses is to send mail to\n"
"bridges@torproject.org with the line \"get bridges\" by itself in the body\n"
"of the mail. However, so we can make it harder for an attacker to learn\n"
"lots of bridge addresses, you must send this request from a gmail or\n"
"yahoo account."
msgstr ""
"Eine weitere Moeglichkeit Bridge-Adressen zu finden ist, eine Email\n"
"an bridges@torproject.org mit der Zeile \"get bridges\" zu Schreiben.\n"
"Diesen Reqeuest muessen Sie allerdings von einem GMail oder Yahoo-Account"
"schicken."

#: lib/bridgedb/I18n.py:41
msgid "[This is an automated message; please do not reply.]"
msgstr "[Diese Mail wurde elektronisch erstellt; Bitte nicht antworten.]"

#: lib/bridgedb/I18n.py:43
msgid ""
"Another way to find public bridge addresses is to visit\n"
"https://bridges.torproject.org/. The answers you get from that page\n"
"will change every few days, so check back periodically if you need more\n"
"bridge addresses."
msgstr ""
"Eine andere Moeglichkeit neue Bridge-Adressen zu finden besteht darin,\n"
"folgende Web-Adresse zu besuchen:\n"
"https://bridges.torproject.org/. Sie koennen diese Adresse alle paar Tage\n"
"besuchen, um neue Bridge-Adressen zu erhalten."

#: lib/bridgedb/I18n.py:48
msgid "(no bridges currently available)"
msgstr "(Zur Zeit sind keine Bridges verfuegbar)"
